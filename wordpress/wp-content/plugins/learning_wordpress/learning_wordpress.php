<?php
/*
Plugin Name: WordPress Learning  Plugin
Plugin URI:  https://developer.wordpress.org/plugins/the-basics/
Description: Basic WordPress Plugin Header Comment
Version:     20160911
Author:      nidhi
Author URI:  https://developer.wordpress.org/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: wporg
Domain Path: /languages
*/

class package{
	function send_email($post_ID) {
    $me = 'nidhi.chaudhary.mu@gmail.com';
	/*
    mail($me, "post is updated", 
      'I just put something on my blog: http://blog.example.com');
    return $post_ID;
   }
   */
   return $post_ID;
}
/* This function is user for register a cutsom post type like page/posts/comments etc*/
function wporg_custom_post_type()
{
    register_post_type('wporg_product',
                       [
                           'labels'      => [
                               'name'          => __('Products'),
                               'singular_name' => __('Product'),
                           ],
                           'public'      => true,
                           'has_archive' => true,
                       ]
    );
	register_post_type('wporg_courses',
                       [
                           'labels'      => [
                               'name'          => __('Courses'),
                               'singular_name' => __('Course'),
                           ],
                           'public'      => true,
                           'has_archive' => true,
                       ]
    );
}

}
$packObj=new package();
add_action('publish_post', array($packObj, 'send_email'));
add_filter('init', array($packObj, 'wporg_custom_post_type'));



?>
��          �      �       H     I     ^     l     �     �     �     �     �     �     �  o   �  #   W  c   {  �  �  ,   �  &   �  !   �  <   	  #   F     j  )   �  
   �     �  0   �  �   �  Y   �  �   B                                  
                	               Check out User Guide Get Some Help Hey! How's It Going? I've already left a review Leave A Review? Maybe Later Never show again Pause Play Sure! I'd love to! Thank you for using WordPress %s! We hope that you've found everything you need, but if you have any questions: There are no posts  in this slider. We hope you've enjoyed using WordPress %s! Would you consider leaving us a review on WordPress.org? Project-Id-Version: wdps
POT-Creation-Date: 2016-08-17 11:04+0500
PO-Revision-Date: 2016-08-17 13:35+0500
Last-Translator: 
Language-Team: 
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: frontend
X-Poedit-SearchPath-1: sliders-notices.php
 Αναχώρηση Οδηγός χρήσης Πάρτε κάποια βοήθεια Γεια σου! Πώς πάει? Έχω ήδη εγκαταλείψει μια κριτική Αφήστε μία κριτική; Ίσως αργότερα Να μην εμφανιστεί ξανά Παύση Παίξτε Σίγουρος! Θα ήθελα πολύ να! Σας ευχαριστούμε που χρησιμοποιείτε το WordPress %s! Ελπίζουμε ότι έχετε βρει όλα όσα χρειάζεστε, αλλά εάν έχετε οποιεσδήποτε ερωτήσεις: Δεν υπάρχουν δημοσιεύσεις σε αυτό το ρυθμιστικό. Ελπίζουμε ότι έχετε απολαύσει χρησιμοποιώντας WordPress %s! Θα εξετάσει αφήνοντας μας μια κριτική για WordPress.org; 
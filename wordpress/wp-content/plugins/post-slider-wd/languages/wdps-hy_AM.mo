��          �      �       H     I     ^     l     �     �     �     �     �     �     �  o   �  #   W  c   {  �  �  !   �  *   �  3   �  6        N      n  1   �  
   �  
   �  %   �  �   �  >   �  �   "                                  
                	               Check out User Guide Get Some Help Hey! How's It Going? I've already left a review Leave A Review? Maybe Later Never show again Pause Play Sure! I'd love to! Thank you for using WordPress %s! We hope that you've found everything you need, but if you have any questions: There are no posts  in this slider. We hope you've enjoyed using WordPress %s! Would you consider leaving us a review on WordPress.org? Project-Id-Version: wdps
POT-Creation-Date: 2016-08-17 11:07+0500
PO-Revision-Date: 2016-08-17 13:46+0500
Last-Translator: 
Language-Team: 
Language: hy_AM
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: frontend
X-Poedit-SearchPath-1: sliders-notices.php
 Ստուգել ուղեցույց Ստանալ որոշ Օգնություն Hey! Ինչպես է այն պատրաստվում. Ես արդեն հեռացել է վերանայման Կազմակերպության. Միգուցե ավելի ուշ Երբեք ցույց են տալիս, կրկին Դադար Սկսել Կներես! Ես սիրում եմ! Շնորհակալություն օգտագործելով WordPress %s! Հուսով ենք, որ դուք գտել այն ամենը, ինչ Ձեզ անհրաժեշտ է, բայց, եթե դուք ունեք որեւէ հարց: Չկան հաղորդագրություններ Այս slider. Հուսով ենք, որ դուք վայելում օգտագործելով WordPress %s! Ցանկանում եք համարում թողնելով մեզ մի վերանայման վերաբերյալ WordPress.org. 
��          �      �       H     I     ^     l     �     �     �     �     �     �     �  o   �  #   W  c   {  �  �     �     �     �     �     �     �               #     *  u   B  &   �  �   �                                  
                	               Check out User Guide Get Some Help Hey! How's It Going? I've already left a review Leave A Review? Maybe Later Never show again Pause Play Sure! I'd love to! Thank you for using WordPress %s! We hope that you've found everything you need, but if you have any questions: There are no posts  in this slider. We hope you've enjoyed using WordPress %s! Would you consider leaving us a review on WordPress.org? Project-Id-Version: wdps
POT-Creation-Date: 2016-08-17 11:09+0500
PO-Revision-Date: 2016-08-17 13:50+0500
Last-Translator: 
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: frontend
X-Poedit-SearchPath-1: sliders-notices.php
 Check out Gebruikershandleiding Get Some Help Hey! Hoe gaat het? Ik heb al een recensie Laat een review? Misschien later Meer tonen nooit Pauze Spelen Tuurlijk! Ik zou graag! Dank u voor het gebruiken van WordPress %s! Wij hopen dat u alles wat u nodig hebt gevonden, maar als je vragen hebt: Er zijn geen berichten in deze slider. We hopen dat je hebt genoten van het gebruik van WordPress %s! Zou u overwegen het verlaten van ons een review op WordPress.org? 
��          �            h     i     ~     �     �     �     �     �     �     �     �  o     #   w  6   �  c   �  �  6  B   4     w     �  #   �     �     �  *     
   ?     J  #   W  �   {  >   S  B   �  �   �                                                         	       
                Check out User Guide Get Some Help Hey! How's It Going? I've already left a review Leave A Review? Maybe Later Never show again Pause Play Sure! I'd love to! Thank you for using WordPress %s! We hope that you've found everything you need, but if you have any questions: There are no posts  in this slider. There is no slider selected or the slider was deleted. We hope you've enjoyed using WordPress %s! Would you consider leaving us a review on WordPress.org? Project-Id-Version: wdps
POT-Creation-Date: 2016-08-17 10:57+0500
PO-Revision-Date: 2016-08-17 10:57+0500
Last-Translator: 
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: frontend
X-Poedit-SearchPath-1: sliders-notices.php
 Проверьте Руководство пользователя Получить помощь Привет! Как дела? Я уже оставил отзыв Оставить отзыв? Может быть позже Никогда снова показать Пауза Начать Конечно! Я бы хотел! Благодарим Вас за использование WordPress %s! Мы надеемся, что Вы нашли все, что нужно, но если у вас есть какие-либо вопросы: Там нет сообщений в этом слайдера. Слайдер не выбрана или была удалена. Мы надеемся, что вам понравилось, используя WordPress %s! Считаете ли вы, оставив нам свой отзыв о WordPress.org? 
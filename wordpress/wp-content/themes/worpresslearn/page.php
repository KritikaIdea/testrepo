<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<section id="about">
<div class="container">
	<div class="row">
	<?php if(has_post_thumbnail() ){ ?>
		<div class="col-sm-6">
				<?php
					the_post_thumbnail( 'medium_large' );
					?>
			</div>
			<?php }?>
			<div class="<?=(has_post_thumbnail())?'col-sm-6':'col-sm-12'?>" style="text-align:center;">
			<h2 class="section-heading"><?php the_title(); ?></h2>
			<hr>
					<?php
					
				// Start the loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					get_template_part( 'template-parts/content', 'page' );
					the_content();
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}

					// End of the loop.
				endwhile;
				?>

			

			<?php //get_sidebar( 'content-bottom' ); ?>
	</div>
</div>
</div>
</section>
<!--<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>

	</main>

	<?php get_sidebar( 'content-bottom' ); ?>

</div>--><!-- .content-area -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage wordpresslearn
 * @since wordpresslearn 1.0
 */
 wp_footer(); 
?>
<!-- Section Footer -->
<section class="bg-dark">
<div class="container">
<div class="row">
	<div class="col-md-12 text-center">
		<h1 class="bottombrand wow flipInX">Hubris Music</h1>
		<p>
			&copy; Hubris Music 2016
		</p>
	</div>
</div>
</div>
</section>

<!-- jQuery -->
<script src="<?php echo get_template_directory_uri() ?>/js/jquery.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/parallax.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/jquery.easing.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/common.js"></script>
</body>
</html>
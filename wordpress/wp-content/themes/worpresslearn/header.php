<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage wordpresslearn
 * @since wordpresslearn 1.0
 */
 global $post; // if outside the loop
 $pages = get_pages(); 

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
 
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>HUBRIS MUSIC</title>
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css" type="text/css">
<!-- Custom Fonts -->
<link href='https://fonts.googleapis.com/css?family=Mrs+Sheppards%7CDosis:300,400,700%7COpen+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800;' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/font-awesome.min.css" type="text/css">
<!-- Plugin CSS -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.min.css" type="text/css">
<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/style.css" type="text/css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php wp_head(); ?>
</head>
<body id="page-top">
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
<div class="container">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand page-scroll" href="#page-top"><img src="<?php echo get_template_directory_uri() ?>/img/logo.png" alt="logolayana"></a>
	</div>
	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav navbar-right">
			<li class="item active">
				<a href="<?php echo get_site_url();?>">Home</a>
			</li>
			<?php foreach ( $pages as $page ) { 
				/* get all childs of a page */
				$mypages = get_pages( array( 'child_of' => $page->ID, 'sort_column' => 'post_date', 'sort_order' => 'desc' ) );?>
				<li class="item">
				<a href="<?php echo esc_url(get_permalink($page->ID) ); ?>"><?php echo $page->post_title;?></a>
				<!--<ul>
				<?php foreach ($mypages as $mpage ) { ?>
					<li class="item">
					<a href="<?php echo $mpage->guid;?>"><?php echo $mpage->post_title;?></a>
					</li>
				<?php }?>
				</ul>-->
				</li>
				<?php } ?>
			
			
		</ul>
	</div>
	<!-- /.navbar-collapse -->
</div>
<!-- /.container -->
</nav>
<div class="carousel intro slide">
<?php echo do_shortcode( '[carousel_slide id="111"]' ); ?>
</div>
<!-- Section Intro Slider -->

<!-- /.carousel -->
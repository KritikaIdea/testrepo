<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage wordpresslearn
 * @since wordpresslearn 1.0
 */

get_header(); ?>
<!-- Section About
================================================== -->
<section id="about">
<div class="container">
	<div class="row">
		<div class="col-lg-10 col-lg-offset-1 text-center">
			<h2 class="section-heading">HAVE YOU EVER WANTED TO PLAY?</h2>
			<hr>
			<p>
				 Have you ever dreamt of being able to pluck your favourite soulful folk ballad? Or jam out an amazing Electric Guitar solo? Do you want to play along with other Musicians but have never been able to get your head around the magic that seems to be taking place?
			</p>
			<p>
				 Maybe your confidence was dented because you tried to teach yourself or have had lessons in the past that didn�t go anywhere? Does your child show an interest/aptitude for music that you would like to help them develop?
			</p>
			<p>
				The truth is anyone can play a musical instrument in whatever way they want!, perhaps that sounds like a grandiose claim but truly all it takes is the desire to do it and the right person to steer and nurture that desire. <u>I've seen it happen and have had the privilege of being a part of that process.</u>
			</p>
	</div>
</div>
</div>
</section>

<!-- one video section -->

<section class="bg-primary">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<!-- add video here -->
				<img src="img/sddefault.jpg" width="448" height="252">
				<!-- /add video here -->
			</div>
			<div class="col-sm-6">
				<p>
					Chances are that if you tried and failed, it wasn�t your fault, it was the result of somebody not recognising the type of learner you are or not driving the lesson in a manner you were able to focus on and process &ndash; <em>we are all different types of learner be it <strong>kinaesthetic</strong></em> (<u>learning best through �hands on� experience</u>), <em><strong>visual</strong> </em>(<u>looking at graphics, watching a demonstration or reading would help optimise your learning</u>) or <em><strong>auditory</strong></em> (<u>reciting information out loud or listening to explanations would work best here</u>) everybody has a preference for one or a combination of the above learning styles, most of the time, without realising it.
				</p>
			</div>
		</div>
	</div>
</section>
<!-- /one video section -->
<!-- 2nd video -->
<section class="">
	<div class="container">
		<div class="row">			
			<div class="col-sm-6">
				<p>
					My lessons utilise all three of these recognised learning styles and I as a tutor will be looking to customise the lessons based on what method or combination of methods suits you best, this is what I mean when I say THEY ARE YOUR LESSONS, AND ARE ENTIRELY TAILORED TO YOUR LEVEL OF EXPERIENCE, YOUR TASTES AND YOUR GOALS!...
				</p>
				<p>
					This is the philosophy HUBRIS music is based on, we believe you can do it and as long as you have the desire, its our job to help channel that desire in the right direction � to build confidence, belief and skill as a result.
				</p>
					Although the structure of your lessons will develop over time based on you the learner, there is scaffolding upon which that learning will be hung upon:
				</p>
			</div>
			<div class="col-sm-6">
				<!-- add video here -->
				<img src="img/sddefault.jpg" width="448" height="252">
				<!-- /add video here -->
			</div>
		</div>
	</div>
</section>
<!-- /2nd video -->
	<!-- three pillars -->
	<section class="bg-dark">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading">The Three Pillars of Music Tuition</h2>
					<hr class="primary">
					<p>Although the Three Pillars and the various learning types apply to all people and age groups there is a slightly different approach to be taken when teaching <a href="/index.php/first-note-music-tuition-for-kids.html">Young Learners</a> which differs from that used with <a href="">Adult Learners</a>:</p>
				</div>
			</div>
		</div>
	<?php $posts = get_posts();
	 /*echo "<pre>";
	print_r($posts); */
	foreach($posts as $post){ ?>
	<div class="col-md-4 no-padding teambox">
		<div class="team-thumb overlay-image view-overlay">
			<img class="img-responsive" alt="" src="img/music-1.jpg">
			<div class="mask team_quote">
				 <div class="port-zoom-link">
					<p>
						<h2><?php echo $post->post_title;?></h2>
					</p>
				</div> 
			</div>
		</div>
		<p><h2><?php echo $post->post_title;?></h2><br><?php echo $post->post_content;?></p>

	</div>
	<?php }?>
	
	
	<div class="clearfix"></div>
	</section>
	<div class="clearfix"></div>
	<!-- /three pillars -->
	<!-- young learners -->
	<section class="">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading">Young Learners</h2>
					<hr>
					<br >
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">	
				<div class="col-sm-4">
					<!-- add video here -->
					<img width="448" height="252" src="img/sddefault.jpg">
					<!-- /add video here -->
				</div>
				<div class="col-sm-8">
					<p>
						For young learners (children aged 4 and upwards) learning would involve <em>bringing the Three Pillars into play using music games, songs that are <strong>fun to play</strong> but develop technical skill at the same time, games that teach the musical alphabet, activities that teach chords and so</em> <em>on</em> all of which are designed to help identify the kind of learning that works best for them while building in the different aspects of the Three Pillars�
					</p>
					<p>The idea is always to engage the child, to make it interesting and fun for them by using their favourite songs/tunes and other easily recognisable songs <strong>while learning in a structured way which can lead to accredited gradings</strong> (<u>Music Gradings are accredited qualifications that can contribute to things like university applications</u>).</p>
					<p>What I've written here barely scratches the surface so If you�re interested in tuition for a young learner then please <a href="/index.php/first-note-music-tuition-for-kids.html">check out our First Note Music Tuition for kids</a>.&nbsp;</p>
				</div>
				
			</div>
		</div>
	</section>
	<!-- /young learners -->
	<!-- HUBRIS Music Advanced Guitar Coaching -->
	<section class="bg-dark">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1 text-center">
					<h2 class="section-heading">HUBRIS Music Advanced Guitar Coaching</h2>
					<hr>
					<br>
					<p>
						 Do you want to fine tune your practice sessions? Would you like to add abit of variety to those outdated licks and phrases? Tired of using the same scale in the same static position OR ever wondered how professionals seamlessly blend sweep picking, legato and tapping phrases into their improvisations on the spur of the moment? Do you want to break your Speed Barrier?
					</p>
					<p>
						If you�re already a seasoned veteran of the Guitar but have hit a musical brick wall then why not see what a Grade 8, professional level Guitar player with over 20 yrs of experience can offer you. 
						<a href="/index.php/hubris-music-advanced-guitar-coaching.html" class="btn btn-primary pull-right" style="padding:5px;">Learn more</a>
					</p>
					
			</div>
		</div>
	  </div>
	</section>
	<!-- HUBRIS Music Advanced Guitar Coaching -->
	<!-- content -->
	<section>
		<div class="container text-center">
			<p>
				Lastly I would just like to say that if you�ve read this far then thank you for reading and why not call, text, email or message me through the site for a chat or just to let me know what you think of the site .
				<br><br>
				There's never a better time than now to finally take that step and turn desire into reality.
				<br><br>

				Hope to hear from you soon.
				<br>

				Matt � (HUBRIS Music - First Note Tuition)
			</p>
		</div>
					
	</section>
	<!-- content -->
<!-- Section Social -->
<section id="social" class="parallax parallax-image" style="background-image:url(img/Music-Wallpaper.jpg);">
<div class="overlay" style="background:#222;opacity:0.5;">
</div>
<div class="wrapsection">
<div class="container">
	<div class="parallax-content">
		<div class="row wow fadeInLeft">
			<div class="col-md-3">
				<div class="funfacts text-center">
					<div class="icon">
						<a href="#"><i class="fa fa-twitter"></i></a>
					</div>
					<h4>Twitter</h4>
				</div>
			</div>
			<div class="col-md-3">
				<div class="funfacts text-center">
					<div class="icon">
						<a href="#"><i class="fa fa-facebook"></i></a>
					</div>
					<h4>Facebook</h4>
				</div>
			</div>
			<div class="col-md-3">
				<div class="funfacts text-center">
					<div class="icon">
						<a href="#"><i class="fa fa-google"></i></a>
					</div>
					<h4>Google</h4>
				</div>
			</div>
			<div class="col-md-3">
				<div class="funfacts text-center">
					<div class="icon">
						<a href="#"><i class="fa fa-youtube"></i></a>
					</div>
					<h4>Youtube</h4>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</section>
<div class="clearfix">
</div>

<!-- Section Contact -->
<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 text-center">
				<h2 class="section-heading">Subscribe to Our Newsletter</h2>
				<hr class="primary">
				<br>
				<div class="regularform">
					<div class="done">
						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">�</button>
							Your message has been sent. Thank you!
						</div>
					</div>
					<form method="post" action="" id="contactform" class="text-left">
						<input name="name" type="text" class="col-sm-12" placeholder="Your Name *"><br>
						<input name="email" type="email" class="col-sm-12" placeholder="E-mail address *"><br>
						<input type="submit"  class="contact submit btn btn-primary btn-xl" value="Subscribe">
					</form>
				</div>				
			</div>
			<div class="col-sm-6 text-center">
				<h2 class="section-heading">CONTACT <b>US</b></h2>
				<hr class="primary">
				<br>
				<div class="regularform">
					<div class="done">
						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">�</button>
							Your message has been sent. Thank you!
						</div>
					</div>
					<form method="post" action="" id="contactform" class="text-left">
						<input name="name" type="text" class="col-sm-6 norightborder" placeholder="Your Name *">
						<input name="phone" type="text" class="col-sm-6" placeholder="Your phone no. *"><br>
						<input name="email" type="email" class="col-sm-12" placeholder="E-mail address *"><br>
						<textarea name="comment" class="col-md-12" placeholder="Message *"></textarea>
						<input type="submit" id="submit" class="contact submit btn btn-primary btn-xl" value="Send message">
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
	<!--<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<?php if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
			<?php endif; ?>

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'content', get_post_format() );

			// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
				'next_text'          => __( 'Next page', 'twentyfifteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>

		</main>
	</div>--><!-- .content-area -->

<?php get_footer(); ?>

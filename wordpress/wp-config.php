<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_test_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7gk& (`pzxsS3RJS4qIP(qRI/h4!a =7c]HPntiC(#U|aEp>yBpj-5S[)2HzA.qZ');
define('SECURE_AUTH_KEY',  '9O5NN2FNL{qGgEf+`yAyyC)d#bGtLiW[Y%0?wat]dnI1tsK1SWdqu~RhI0^ztrH=');
define('LOGGED_IN_KEY',    '&V%czW@Ll-+#Xp,iwL9ys4qg6*;6<wi7dx4X[kLs*CUe-0Tru$CfHDgWGk$]MBJu');
define('NONCE_KEY',        '/t1%GmB*+7B54Sgl9)3oNiC#u)PLrLUne`^Wyd5[7P/)Pe*Q?0>UedFRg$jKOk@A');
define('AUTH_SALT',        'O3a~htH$1+MXQv55B]sNM[X E6oSib{f<N*uLvq&YW}aq4e=be8?}3&Xtf_hFdrf');
define('SECURE_AUTH_SALT', '$Y{;|!G>Pb^dWBe zL|oZ#5[lPlYEcveHnx.N Jk1Ck=u[o%Q5Oc2ZU:PH3+r6~!');
define('LOGGED_IN_SALT',   '=^@#(s5A,CJC`]W=/Ehf>gIayg.YAdhw(Mz+F6s)lo/Q*c-RxNgT:jP2w@k,JaQ(');
define('NONCE_SALT',       '+B:O$tz#[CUA+~]Nl>_4p3=}EEllju!!3XWDY=^Y?Y[Q[Kt9h?<0,mbk=)c>cHo=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
